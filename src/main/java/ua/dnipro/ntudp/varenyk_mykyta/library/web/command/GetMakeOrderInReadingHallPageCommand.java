package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Returns a page with order in reading hall form for a user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetMakeOrderInReadingHallPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetMakeOrderInReadingHallPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetMakeOrderInReadingHallPageCommand started");

        int id = Integer.parseInt(req.getParameter(Fields.ENTITY_ID));

        req.setAttribute("id",id);

        LOGGER.debug("book id -> {}",id);

        LOGGER.debug("GetMakeOrderInReadingHallPageCommand finished");
        return Path.PAGE_USER_MAKE_ORDER_IN_READING_HALL_PAGE;
    }
}
