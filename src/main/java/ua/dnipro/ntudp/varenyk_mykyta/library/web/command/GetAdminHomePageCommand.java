package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Return a home page of admin user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetAdminHomePageCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(GetAdminHomePageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetAdminHomePageCommand executed");
        return Path.PAGE_ADMIN_HOME_PAGE;
    }
}
