package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Returns a page with orders of a specified user for a librarian user.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetOrdersForUserPageCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(GetOrdersForUserPageCommand.class);

    private OrderDao orderDao;

    @Autowired
    public GetOrdersForUserPageCommand(OrderDao orderDao){
        this.orderDao = orderDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetOrdersForUserPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null){
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("currentPage -> {}",currentPage);

        int id = Integer.parseInt(req.getParameter("id"));

        LOGGER.debug("user id -> {}",id);

        int rows = orderDao.getOrdersAmountForUser(id);

        int numOfPages = (int) Math.ceil(rows * 1.0/recordsPerPage);

        LOGGER.debug("numOfPages % recordsPerPage > 0 -> {}",numOfPages % recordsPerPage > 0);

        LOGGER.debug("numOfPages -> {}",numOfPages);

        List<UserOrderBean> beans = orderDao.getUserOrderBeansByUserId(id,languageCode,currentPage ,recordsPerPage);

        LOGGER.debug("userOrderBeans with limit:{} and offset:{} -> {}",currentPage,recordsPerPage,beans);

        req.setAttribute("beans",beans);

        req.setAttribute("currentPage",currentPage);

        req.setAttribute("recordsPerPage",recordsPerPage);

        req.setAttribute("numOfPages",numOfPages);

        req.setAttribute("id",id);

        LOGGER.debug("GetOrdersForUserPageCommand finished");

        return Path.PAGE_LIBRARIAN_LIST_ORDERS_FOR_USER;
    }
}
