package ua.dnipro.ntudp.varenyk_mykyta.library;

import org.junit.Test;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

public class FunctionsTest {

    @Test
    public void shouldReturnDifferenceBetweenSpecifiedTimeAndNow(){
        LocalDateTime approvedTime = LocalDateTime.parse("2020-07-07T10:17:17");
        long delay = Functions.getDelay(approvedTime);
        assertEquals(delay,Functions.getDelay(approvedTime));
    }

    @Test
    public void shouldReturnNumberOfOrderedBooks(){
        DataSource dataSource = DBManager.getDataSourceForTests();

        OrderDao orderDao = new OrderDao(dataSource);

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans("en");

        int count = Functions.countOrderedBooks(userOrderBeans,1);

        assertEquals(count,Functions.countOrderedBooks(userOrderBeans,1));
    }
}