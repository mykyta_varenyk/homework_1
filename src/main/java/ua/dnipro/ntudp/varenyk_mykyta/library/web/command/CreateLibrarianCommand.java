package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Create a librarian.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class CreateLibrarianCommand extends Command {
    private static Logger LOGGER = LogManager.getLogger(CreateLibrarianCommand.class);

    private UserDao userDao;

    @Autowired
    public CreateLibrarianCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("CreateLibrarianCommand started");

        HttpSession session = req.getSession();

        String languageCode,errorMessage;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}",languageCode);

        if (!Util.validateEmail(req.getParameter(Fields.USER_EMAIL))){
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                            .getString("label.emailPatternError");

            session.setAttribute("emailPatternError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
        }

        if (!Util.validatePassword(req.getParameter(Fields.USER_PASSWORD))){
            errorMessage = ResourceBundle.getBundle("text",Locale.forLanguageTag(languageCode))
                           .getString("label.passwordPatternError");

            session.setAttribute("passwordPatternError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
        }

        try {
            userDao = new UserDao(DBManager.getDataSource());
        } catch (NamingException e) {
            e.printStackTrace();
        }

        if (userDao.loginIsPresent(req.getParameter(Fields.USER_LOGIN))){
            errorMessage = ResourceBundle.getBundle("text",Locale.forLanguageTag(languageCode))
                    .getString("label.loginIsPresentError");

            session.setAttribute("loginIsPresentError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
        }

        if (userDao.emailIsPresent(req.getParameter(Fields.USER_EMAIL))){
            errorMessage = ResourceBundle.getBundle("text",Locale.forLanguageTag(languageCode))
                          .getString("label.emailIsPresentError");

            session.setAttribute("emailIsPresentError",errorMessage);

            LOGGER.error("errorMessage -> {}",errorMessage);

            return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
        }

        User user = getUser(req);

        LOGGER.debug("librarian to be inserted -> {}",user);

        boolean result = userDao.createLibrarian(user);

        LOGGER.debug("librarian inserted -> {}",result);

        LOGGER.debug("CreateLibrarianCommand finished");

        return Path.PAGE_ADMIN_USERS_PAGE_REDIRECT;
    }

    private User getUser(HttpServletRequest request){
        User user = new User();

        user.setLogin(request.getParameter(Fields.USER_LOGIN));
        user.setPassword(request.getParameter(Fields.USER_PASSWORD));
        user.setFirstName(request.getParameter(Fields.USER_FIRST_NAME));
        user.setLastName(request.getParameter(Fields.USER_LAST_NAME));
        user.setEmail(request.getParameter(Fields.USER_EMAIL));
        return user;
    }
}
