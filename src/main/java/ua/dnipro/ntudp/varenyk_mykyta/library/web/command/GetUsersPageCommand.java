package ua.dnipro.ntudp.varenyk_mykyta.library.web.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.BookDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Returns a page with all users for admin.
 *
 * @author Mykyta Varenyk
 *
 */
@Service
public class GetUsersPageCommand extends Command{
    private static Logger LOGGER = LogManager.getLogger(GetUsersPageCommand.class);

    private UserDao userDao;

    @Autowired
    public GetUsersPageCommand(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.debug("GetUsersPageCommand started");

        HttpSession session = req.getSession();

        String languageCode;

        if (session != null){
            languageCode = (String) session.getAttribute("language");
        }else {
            languageCode = "en";
        }

        LOGGER.debug("languageCode -> {}",languageCode);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null){
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("page -> {}",currentPage);

        BookDao bookDao = null;
        try {
            bookDao = new BookDao(DBManager.getDataSource());
        } catch (NamingException e) {
            LOGGER.error("NamingException in GetUsersPageCommand -> {}",e);
        }

        List<User> users = userDao.getUsers(currentPage,recordsPerPage);

        LOGGER.debug("users with limit:{} and offset:{} -> {}",currentPage,recordsPerPage,users);

        int rows = userDao.getNumberOfUsers();

        LOGGER.debug("rows -> {}",rows);

        int numOfPages = rows/recordsPerPage;

        LOGGER.debug("numOfPages initialized -> {}",numOfPages);

        if (numOfPages % recordsPerPage > 0){
            numOfPages++;
        }

        LOGGER.debug("numOfPages final value -> {}",numOfPages);

        req.setAttribute("users",users);

        req.setAttribute("currentPage",currentPage);

        req.setAttribute("recordsPerPage",recordsPerPage);

        req.setAttribute("numOfPages",numOfPages);

        List<Author> authors = bookDao.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}",authors);

        req.setAttribute("authors",authors);

        LOGGER.debug("GetUsersPageCommand finished");

        return Path.PAGE_ADMIN_USERS_PAGE;
    }
}
