package ua.dnipro.ntudp.varenyk_mykyta.library.web.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.command.GetOrderedBooksPageCommand;

import java.util.List;

@Service
public class OrderService {
    private static Logger LOGGER = LogManager.getLogger(GetOrderedBooksPageCommand.class);

    private OrderDao orderDao;

    @Autowired
    public OrderService (OrderDao orderDao){
        this.orderDao = orderDao;
    }

    public int countOrdersByUserId(int id,int recordsPerPage){
        return ServiceUtil.getNumberOfPages(orderDao.getOrdersAmountForUser(id),recordsPerPage);
    }

    public List<UserOrderBean> getUserOrderBeansWithPagination(int currentPage,int recordsPerPage,String languageCode,int id){
        int rows = orderDao.getOrdersAmountForUser(id);

        LOGGER.debug("rows -> {}",rows);

        int numOfPages = (int) Math.ceil(rows * 1.0/recordsPerPage);

        LOGGER.debug("numOfPages -> {}",numOfPages);

        List<UserOrderBean> userOrderBeans = orderDao.getUserOrderBeansByUserId(id,languageCode,currentPage,recordsPerPage);

        LOGGER.debug("userOrderBeans with limit:{} and offset:{} -> {}",currentPage,recordsPerPage,userOrderBeans);

        return orderDao.getUserOrderBeansByUserId(id,languageCode,currentPage,recordsPerPage);
    }
}
