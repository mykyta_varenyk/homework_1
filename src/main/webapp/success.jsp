<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />

<fm:setBundle basename="text"/>

<html>
<head>
    <title>success</title>
</head>
<body>
<h3>Success</h3>
<a href="/FinalProject/jsp/login.jsp"><fmt:message key="button.login"/></a>

<h:change-language path="/success.jsp"/>
<%@include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
