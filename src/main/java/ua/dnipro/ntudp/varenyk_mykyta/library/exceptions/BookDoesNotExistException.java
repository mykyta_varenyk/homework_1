package ua.dnipro.ntudp.varenyk_mykyta.library.exceptions;

public class BookDoesNotExistException extends Exception {
    public BookDoesNotExistException(){
        super();
    }

    public BookDoesNotExistException(String message){
        super(message);
    }
}
